#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
//! Publish documentation by syncing it.
//!
//! Load a configuration file that describes what tools to use to
//! build the documentation (e.g. running Tox to invoke MkDocs), and
//! what tools to use to sync the documentation to the location where
//! it is published (e.g. running rsync).
//!
//! Invoke the build tool specified in the configuration.
//!
//! Invoke the sync tool specified in the configuration.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#![allow(clippy::pub_use)]

pub mod build;
pub mod config;
pub mod defs;
pub mod sync;

pub use build::{dispatch as build_dispatch, get_default_build_output};
pub use config::ProjectConfig;
pub use defs::Config;
pub use sync::dispatch as sync_dispatch;

#[cfg(test)]
pub mod tests;
