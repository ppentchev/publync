//! Build the documentation using various tools.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use std::path::{Path, PathBuf};

pub mod tox;

use crate::config::{BuildTool, ProjectConfig};
use crate::defs::{Config, Error};

/// The result of a successful build.
#[derive(Debug)]
#[allow(clippy::module_name_repetitions)]
pub struct BuildOutput {
    /// The path to the directory where the documentation was placed.
    path: PathBuf,
}

impl BuildOutput {
    /// The path to the directory where the documentation was placed.
    #[inline]
    #[must_use]
    pub fn path(&self) -> &Path {
        &self.path
    }
}

/// Build the documentation using the specified build tool.
///
/// # Errors
///
/// Propagate errors from [`tox::build`].
#[inline]
pub fn dispatch(cfg: &Config, proj: &ProjectConfig) -> Result<BuildOutput, Error> {
    match *proj.build() {
        BuildTool::Tox(ref tox_cfg) => tox::build(cfg, tox_cfg),
    }
}

/// Get the default build output for syncing.
///
/// # Errors
///
/// None so far.
#[inline]
pub fn get_default_build_output(cfg: &Config, proj: &ProjectConfig) -> Result<BuildOutput, Error> {
    match *proj.build() {
        BuildTool::Tox(ref tox_cfg) => Ok(tox::get_default_build_output(cfg, tox_cfg)),
    }
}
