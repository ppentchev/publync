//! Build the documentation using the Python Tox test runner.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use std::process::Command;

use anyhow::anyhow;
use tracing::info;

use crate::config::BuildToxConfig;
use crate::defs::{Config, Error};

use super::BuildOutput;

/// Get the default build output for syncing.
#[inline]
#[must_use]
pub fn get_default_build_output(cfg: &Config, tox_cfg: &BuildToxConfig) -> BuildOutput {
    BuildOutput {
        path: cfg.project_dir().join(tox_cfg.output_path()),
    }
}

/// Build the documentation using the Python Tox test runner.
///
/// # Errors
///
/// [`Error::ProgramRun`] if the Tox program cannot even be started.
/// [`Error::ProgramFailed`] if the Tox invocation failed or the output directory was not created.
#[allow(clippy::missing_inline_in_public_items)]
pub fn build(cfg: &Config, tox_cfg: &BuildToxConfig) -> Result<BuildOutput, Error> {
    let res = get_default_build_output(cfg, tox_cfg);
    let tox_str = || shell_words::join([tox_cfg.program(), "-e", tox_cfg.environment()]);
    info!(
        "Building the documentation in {output_path} using `{tox_str}`",
        output_path = res.path().display(),
        tox_str = tox_str(),
    );

    #[allow(clippy::print_stdout)]
    if cfg.noop() {
        println!("chdir {project_dir}", project_dir = cfg.project_dir_str());
        println!("{tox_str}", tox_str = tox_str());
        return Ok(res);
    }

    if !Command::new(tox_cfg.program())
        .args(["-e", tox_cfg.environment()])
        .current_dir(cfg.project_dir())
        .status()
        .map_err(|err| Error::ProgramRun(tox_str(), err))?
        .success()
    {
        return Err(Error::ProgramFailed(
            tox_str(),
            anyhow!(
                "{prog} exited with a non-zero code",
                prog = tox_cfg.program()
            ),
        ));
    }

    if !res.path().is_dir() {
        return Err(Error::ProgramFailed(
            tox_str(),
            anyhow!(
                "The {path} directory was not created",
                path = res.path().display()
            ),
        ));
    }
    info!(
        "Built the documentation at {path}",
        path = res.path().display()
    );

    Ok(res)
}
