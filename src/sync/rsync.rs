//! Sync the documentation using the rsync tool.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use std::iter;
use std::process::Command;

use anyhow::{anyhow, Context};
use tracing::info;

use crate::build::BuildOutput;
use crate::config::SyncRsyncConfig;
use crate::defs::{Config, Error};

/// Sync the documentation using the rsync tool.
///
/// # Errors
///
/// [`Error::ProgramRun`] if the rsync program cannot even be started.
/// [`Error::ProgramFailed`] if the rsync invocation failed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn sync(cfg: &Config, rsync_cfg: &SyncRsyncConfig, output: &BuildOutput) -> Result<(), Error> {
    let path_str_bare = output
        .path()
        .to_str()
        .context("Could not represent the docs path as a string")
        .map_err(Error::SystemConfig)?;
    let path_str = format!(
        "{path_str_bare}{slash}",
        slash = if path_str_bare.ends_with('/') {
            ""
        } else {
            "/"
        }
    );
    let remote = format!(
        "{remote}{slash}",
        remote = rsync_cfg.remote(),
        slash = if rsync_cfg.remote().ends_with('/') {
            ""
        } else {
            "/"
        }
    );
    info!("Syncing the documentation from {path_str} to {remote}");

    let rsync_prog = "rsync";
    let rsync_args = ["-avz", "--delete", "--", &path_str, &remote];
    let rsync_str = || shell_words::join(iter::once(rsync_prog).chain(rsync_args));

    #[allow(clippy::print_stdout)]
    if cfg.noop() {
        println!("{rsync_str}", rsync_str = rsync_str());
        return Ok(());
    }

    if !Command::new(rsync_prog)
        .args(rsync_args)
        .current_dir(cfg.project_dir())
        .status()
        .map_err(|err| Error::ProgramRun(rsync_str(), err))?
        .success()
    {
        return Err(Error::ProgramFailed(
            rsync_str(),
            anyhow!("{rsync_prog} exited with a non-zero code"),
        ));
    }
    info!("Sync successful, it seems");
    Ok(())
}
