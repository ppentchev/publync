//! Sync the documentation using various tools.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

pub mod rsync;

use crate::build::BuildOutput;
use crate::config::{ProjectConfig, SyncTool};
use crate::defs::{Config, Error};

/// Sync the documentation using the specified sync tool.
///
/// # Errors
///
/// Propagate errors from [`rsync::sync`].
#[inline]
pub fn dispatch(cfg: &Config, proj: &ProjectConfig, output: &BuildOutput) -> Result<(), Error> {
    match *proj.sync() {
        SyncTool::Rsync(ref rsync_cfg) => rsync::sync(cfg, rsync_cfg, output),
    }
}
