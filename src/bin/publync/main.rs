#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Publish documentation by syncing it.

use std::process::{ExitCode, Termination};

use anyhow::{Context, Result};

use publync::ProjectConfig;

mod cli;

use cli::Mode;

/// The result of the main program or an invoked subcommand.
enum MainResult {
    /// Everything seems fine.
    Ok,
}

impl Termination for MainResult {
    fn report(self) -> ExitCode {
        match self {
            Self::Ok => ExitCode::SUCCESS,
        }
    }
}

/// The main program - parse command-line options, do something about it.
fn main() -> Result<MainResult> {
    match cli::try_parse()? {
        Mode::Build(cfg) => {
            let proj = ProjectConfig::from_config(&cfg)?;
            publync::build_dispatch(&cfg, &proj)?;
            Ok(MainResult::Ok)
        }
        Mode::Sync(cfg) => {
            let proj = ProjectConfig::from_config(&cfg)?;
            let output = publync::get_default_build_output(&cfg, &proj)?;
            publync::sync_dispatch(&cfg, &proj, &output)?;
            Ok(MainResult::Ok)
        }
        Mode::Run(cfg) => {
            let proj = ProjectConfig::from_config(&cfg)?;
            let output = publync::build_dispatch(&cfg, &proj)?;
            publync::sync_dispatch(&cfg, &proj, &output)?;
            Ok(MainResult::Ok)
        }
        #[allow(clippy::print_stdout)]
        Mode::ShowConfig(cfg, no_overrides) => {
            let proj = if no_overrides {
                ProjectConfig::from_config_without_overrides(&cfg)?
            } else {
                ProjectConfig::from_config(&cfg)?
            };
            print!(
                "{config}",
                config = toml::to_string(&proj)
                    .context("Could not serialize the parsed configuration")?
            );
            Ok(MainResult::Ok)
        }
        Mode::Handled => Ok(MainResult::Ok),
    }
}
