// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Parse command-line options for the `publync` tool.

use std::io;

use anyhow::{bail, Context, Result};
use clap::error::ErrorKind as ClapErrorKind;
use clap::Parser;
use clap_derive::{Parser, Subcommand};
use tracing::Level;

use publync::defs::Config;

/// What to show, what to show?
#[derive(Debug, Subcommand)]
enum CliShowCommand {
    /// Show the parsed project configuration along with the applied overrides.
    Config {
        /// Do not apply the overrides before displaying the configuration.
        #[clap(long)]
        no_overrides: bool,
    },
}

/// What to do, what to do?
#[derive(Debug, Subcommand)]
enum CliCommand {
    /// Build the documentation.
    Build,

    /// Sync the built documentation.
    Sync,

    /// Build the documentation, sync it to the remote site.
    Run,

    /// Show various settings.
    Show {
        /// What to show?
        #[clap(subcommand)]
        show_cmd: CliShowCommand,
    },
}

/// publync - publish documentation by syncing it
#[derive(Debug, Parser)]
#[clap(version)]
struct Cli {
    /// The path to the directory containing the configuration overrides.
    #[clap(short('C'), long)]
    config_home: Option<String>,

    /// Debug mode; display even more diagnostic output.
    #[clap(short, long)]
    debug: bool,

    /// No-operation mode; display what would be done.
    #[clap(short('N'), long)]
    noop: bool,

    /// The project directory, if different from the current directory.
    #[clap(short, long)]
    project_dir: Option<String>,

    /// Verbose operation; display diagnostic output.
    #[clap(short, long)]
    verbose: bool,

    /// What to do?
    #[clap(subcommand)]
    cmd: CliCommand,
}

/// What to do, what to do?
#[derive(Debug)]
pub enum Mode {
    /// Build the documentation.
    Build(Config),

    /// Sync the built documentation.
    Sync(Config),

    /// Build the documentation, sync it to the remote site.
    Run(Config),

    /// Display the parsed configuration, with or without the overrides applied.
    ShowConfig(Config, bool),

    /// Help or version requested, handled by the options parser.
    Handled,
}

/// Initialize the logging subsystem provided by the `tracing` library.
fn setup_tracing(verbose: bool, debug: bool) -> Result<()> {
    let sub = tracing_subscriber::fmt()
        .without_time()
        .with_max_level(if debug {
            Level::TRACE
        } else if verbose {
            Level::DEBUG
        } else {
            Level::INFO
        })
        .with_writer(io::stderr)
        .finish();
    #[allow(clippy::absolute_paths)]
    tracing::subscriber::set_global_default(sub).context("Could not initialize the tracing logger")
}

/// Parse the command-line arguments, determine the mode of operation.
///
/// # Errors
///
/// Propagate command-line parsing errors.
pub fn try_parse() -> Result<Mode> {
    let args = match Cli::try_parse() {
        Ok(args) => args,
        Err(err)
            if matches!(
                err.kind(),
                ClapErrorKind::DisplayHelp | ClapErrorKind::DisplayVersion
            ) =>
        {
            err.print()
                .context("Could not display the usage or version message")?;
            return Ok(Mode::Handled);
        }
        Err(err) if err.kind() == ClapErrorKind::DisplayHelpOnMissingArgumentOrSubcommand => {
            err.print()
                .context("Could not display the usage or version message")?;
            bail!("Invalid or missing command-line options");
        }
        Err(err) => return Err(err).context("Could not parse the command-line options"),
    };
    setup_tracing(args.verbose, args.debug)
        .context("Could not initialize the logging infrastructure")?;

    let cfg = Config::from_xdg(args.config_home, args.project_dir)
        .context("Could not initialize the publync configuration")?
        .with_noop(args.noop)
        .with_verbose(args.verbose);
    match args.cmd {
        CliCommand::Build => Ok(Mode::Build(cfg)),
        CliCommand::Sync => Ok(Mode::Sync(cfg)),
        CliCommand::Run => Ok(Mode::Run(cfg)),
        CliCommand::Show { show_cmd } => match show_cmd {
            CliShowCommand::Config { no_overrides } => Ok(Mode::ShowConfig(cfg, no_overrides)),
        },
    }
}
