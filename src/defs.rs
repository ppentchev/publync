//! Common definitions for the `publync` crate.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use std::io::Error as IoError;
use std::path::{Path, PathBuf};

use anyhow::{Context, Error as AnyError};
use thiserror::Error;
use xdg::BaseDirectories;

/// An error that occurred during the `publync` operation.
#[derive(Debug, Error)]
#[non_exhaustive]
#[allow(clippy::error_impl_error)]
pub enum Error {
    /// Something went really, really wrong.
    #[error("fnmatch-regex internal error")]
    InternalError(#[source] AnyError),

    /// Missing or invalid `[format.version]` section in the configuration.
    #[error("No publync [format.version] in the {0} file")]
    FileParseNoFormatVersion(String),

    /// Could not parse an input file as valid TOML.
    #[error("Could not parse the {0} file as valid publync configuration")]
    FileParsePublync(String, #[source] AnyError),

    /// Could not parse an input file as valid TOML.
    #[error("Could not parse the {0} file as valid TOML")]
    FileParseToml(String, #[source] AnyError),

    /// Could not read an input file.
    #[error("Could not read the {0} file")]
    FileRead(String, #[source] IoError),

    /// No configuration file found in the project directory.
    #[error("No configuration found in publync.toml or pyproject.toml in the {0} directory")]
    NoConfig(String),

    /// Some known missing functionality.
    #[error("Not implemented yet: {0}")]
    NotImplemented(String),

    /// An executed program failed.
    #[error("The `{0}` command failed")]
    ProgramFailed(String, #[source] AnyError),

    /// Could not start a program.
    #[error("Could not run `{0}`")]
    ProgramRun(String, #[source] IoError),

    /// Something is not set up correctly.
    #[error("System configuration problem")]
    SystemConfig(#[source] AnyError),
}

/// Runtime configuration for the `publync` routines.
#[derive(Debug)]
pub struct Config {
    /// The path to the configuration directory in the user's home directory.
    config_home: PathBuf,

    /// No-operation mode; display what would be done.
    noop: bool,

    /// The project directory as an absolute path.
    project_dir: PathBuf,

    /// Verbose operation; display diagnostic output.
    verbose: bool,
}

impl Config {
    /// The path to the configuration directory in the user's home directory.
    #[inline]
    #[must_use]
    pub fn config_home(&self) -> &Path {
        &self.config_home
    }

    /// No-operation mode; display what would be done.
    #[inline]
    #[must_use]
    pub const fn noop(&self) -> bool {
        self.noop
    }

    /// The project directory as an absolute path.
    #[inline]
    #[must_use]
    pub fn project_dir(&self) -> &Path {
        &self.project_dir
    }

    /// The project directory as a human-readable string representation.
    #[inline]
    #[must_use]
    pub fn project_dir_str(&self) -> String {
        format!("{path}", path = self.project_dir.display())
    }

    /// Verbose operation; display diagnostic output.
    #[inline]
    #[must_use]
    pub const fn verbose(&self) -> bool {
        self.verbose
    }

    /// Return a new [`Config`] object with the specified no-operation setting.
    #[inline]
    #[must_use]
    pub fn with_noop(self, noop: bool) -> Self {
        Self { noop, ..self }
    }

    /// Return a new [`Config`] object with the specified project directory.
    #[inline]
    #[must_use]
    pub fn with_project_dir(self, project_dir: PathBuf) -> Self {
        Self {
            project_dir,
            ..self
        }
    }

    /// Return a new [`Config`] object with the specified verbosity level.
    #[inline]
    #[must_use]
    pub fn with_verbose(self, verbose: bool) -> Self {
        Self { verbose, ..self }
    }

    /// Build a [`Config`] object using the specified configuration paths.
    ///
    /// # Errors
    ///
    /// [`Error::SystemConfig`] if the project directory cannot be canonicalized.
    #[inline]
    pub fn from_paths<PProjDir: AsRef<Path>>(
        config_home: PathBuf,
        project_dir: Option<PProjDir>,
    ) -> Result<Self, Error> {
        Ok(Self {
            config_home,
            noop: false,
            project_dir: project_dir
                .map_or_else(
                    || Path::new(".").canonicalize(),
                    |path| path.as_ref().canonicalize(),
                )
                .context("Could not resolve the project directory")
                .map_err(Error::SystemConfig)?,
            verbose: false,
        })
    }

    /// Build a [`Config`] object using the XDG default paths.
    ///
    /// # Errors
    ///
    /// [`Error::SystemConfig`] if the XDG environment setup fails.
    ///
    /// Propagate errors from [`Config::from_paths`].
    #[inline]
    pub fn from_xdg(
        config_home: Option<String>,
        project_dir: Option<String>,
    ) -> Result<Self, Error> {
        let xdg_dirs = BaseDirectories::new()
            .context("Could not examine the XDG environment settings")
            .map_err(Error::SystemConfig)?;
        let res_config_home = config_home.map_or_else(
            || xdg_dirs.get_config_home(),
            |home| Path::new(&home).to_path_buf(),
        );
        Self::from_paths(res_config_home, project_dir)
    }
}
