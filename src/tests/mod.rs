//! Test parsing and syncing.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */
// This is a test suite.
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::panic_in_result_fn)]
#![allow(clippy::print_stdout)]
#![allow(clippy::unwrap_used)]

use std::fs;
use std::path::Path;

use anyhow::{Context, Result};
use tracing::debug;
use tracing_test::traced_test;

use crate::config::{BuildTool, BuildToxConfig, ProjectConfig, SyncTool};
use crate::defs::{Config, Error};

#[test]
#[traced_test]
pub fn test_project_config_fail() -> Result<()> {
    println!();
    assert!(matches!(
        ProjectConfig::from_config_without_overrides(&Config::from_paths(
            Path::new("/nonexistent").to_path_buf(),
            Some("/")
        )?),
        Err(Error::NoConfig(_))
    ));

    let tempd = tempfile::Builder::new()
        .prefix("proj-config-test.")
        .tempdir()
        .context("Could not create a temporary directory")?;
    debug!("tempd={tempd}", tempd = tempd.path().display());
    let cfg = Config::from_paths(Path::new("/nonexistent").to_path_buf(), Some(tempd.path()))?;
    let lync_path = tempd.path().join("publync.toml");

    let write_file = |value| {
        fs::write(&lync_path, value)
            .with_context(|| format!("Could not create {path}", path = lync_path.display()))
    };
    let proj = || ProjectConfig::from_config_without_overrides(&cfg);

    // Start with invalid TOML.
    write_file(">\n")?;
    assert!(matches!(proj(), Err(Error::FileParseToml(_, _))));

    // Valid TOML, no format.version at all.
    write_file("[something]\n")?;
    assert!(matches!(proj(), Err(Error::FileParseNoFormatVersion(_))));

    // No format.version.major.
    write_file("[format.version]\nminor=1\n")?;
    assert!(matches!(proj(), Err(Error::FileParseNoFormatVersion(_))));

    // No format.version.minor.
    write_file("[format.version]\nmajor=42\n")?;
    assert!(matches!(proj(), Err(Error::FileParseNoFormatVersion(_))));

    // Not u32 values.
    write_file("[format.version]\nmajor=-42\nminor=-1\n")?;
    assert!(matches!(proj(), Err(Error::FileParseNoFormatVersion(_))));

    // Unexpected format version.
    write_file("[format.version]\nmajor=42\nminor=1\n")?;
    assert!(matches!(proj(), Err(Error::FileParsePublync(_, _))));

    // Correct format version, no build tool specified.
    write_file("[format.version]\nmajor=0\nminor=1\n")?;
    assert!(matches!(proj(), Err(Error::FileParsePublync(_, _))));

    // Invalid build tool specified.
    write_file("[format.version]\nmajor=0\nminor=1\n\n[build.whee]\n")?;
    assert!(matches!(proj(), Err(Error::FileParsePublync(_, _))));

    // Invalid Tox environment name specified.
    write_file("[format.version]\nmajor=0\nminor=1\n\n[build.whee]\nenvironment=false\n")?;
    assert!(matches!(proj(), Err(Error::FileParsePublync(_, _))));

    Ok(())
}

#[test]
#[traced_test]
fn test_project_config_publync_ok() -> Result<()> {
    println!();
    let tempd = tempfile::Builder::new()
        .prefix("proj-config-test.")
        .tempdir()
        .context("Could not create a temporary directory")?;
    debug!("tempd={tempd}", tempd = tempd.path().display());
    let cfg = Config::from_paths(Path::new("/nonexistent").to_path_buf(), Some(tempd.path()))?;
    let lync_path = tempd.path().join("publync.toml");

    let write_file = |value| {
        fs::write(&lync_path, value)
            .with_context(|| format!("Could not create {path}", path = lync_path.display()))
    };
    let proj = || ProjectConfig::from_config_without_overrides(&cfg);

    {
        write_file(
            r#"
[format.version]
major = 0
minor = 1

[build.tox]

[sync.rsync]
remote = "jrl@nowhere"
"#,
        )?;
        let res = proj()?;
        assert_eq!(*res.build(), BuildTool::Tox(BuildToxConfig::default()));
        match *res.sync() {
            SyncTool::Rsync(ref rsync_cfg) => assert_eq!(rsync_cfg.remote(), "jrl@nowhere"),
        }
    }

    {
        write_file(
            r#"
[format.version]
major = 0
minor = 1

[build.tox]
environment = "something"

[sync.rsync]
remote = "somewhere@in.time"
"#,
        )?;
        let res = proj()?;
        match *res.build() {
            BuildTool::Tox(ref tox_cfg) => assert_eq!(tox_cfg.environment(), "something"),
        }
        match *res.sync() {
            SyncTool::Rsync(ref rsync_cfg) => assert_eq!(rsync_cfg.remote(), "somewhere@in.time"),
        }
    }
    Ok(())
}

#[test]
#[traced_test]
fn test_project_config_pyproj_ok() -> Result<()> {
    println!();
    let tempd = tempfile::Builder::new()
        .prefix("proj-config-test.")
        .tempdir()
        .context("Could not create a temporary directory")?;
    debug!("tempd={tempd}", tempd = tempd.path().display());
    let cfg = Config::from_paths(Path::new("/nonexistent").to_path_buf(), Some(tempd.path()))?;
    let lync_path = tempd.path().join("pyproject.toml");

    let write_file = |value| {
        fs::write(&lync_path, value)
            .with_context(|| format!("Could not create {path}", path = lync_path.display()))
    };
    let proj = || ProjectConfig::from_config_without_overrides(&cfg);

    {
        write_file(
            r#"
[tool.publync.format.version]
major = 0
minor = 1

[tool.publync.build.tox]

[tool.publync.sync.rsync]
remote = "jrl@nowhere"
"#,
        )?;
        let res = proj()?;
        assert_eq!(*res.build(), BuildTool::Tox(BuildToxConfig::default()));
        match *res.sync() {
            SyncTool::Rsync(ref rsync_cfg) => assert_eq!(rsync_cfg.remote(), "jrl@nowhere"),
        }
    }

    {
        write_file(
            r#"
[tool.publync.format.version]
major = 0
minor = 1

[tool.publync.build.tox]
environment = "something"

[tool.publync.sync.rsync]
remote = "somewhere@in.time"
"#,
        )?;
        let res = proj()?;
        match *res.build() {
            BuildTool::Tox(ref tox_cfg) => assert_eq!(tox_cfg.environment(), "something"),
        }
        match *res.sync() {
            SyncTool::Rsync(ref rsync_cfg) => assert_eq!(rsync_cfg.remote(), "somewhere@in.time"),
        }
    }
    Ok(())
}
