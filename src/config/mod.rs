//! The project-specific configuration for the `publync` routines.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#![allow(clippy::module_name_repetitions)]

use std::fs;
use std::io::ErrorKind;
use std::path::Path;

use anyhow::{anyhow, Context, Error as AnyError};
use serde::Deserialize;
use serde_derive::{Deserialize, Serialize};
use toml::Value as TomlValue;
use tracing::debug;

use crate::defs::{Config, Error};

pub mod build;
pub mod sync;

pub use build::{BuildTool, BuildToolOverrides, BuildToxConfig};
pub use sync::{SyncRsyncConfig, SyncTool, SyncToolOverrides};

/// The major version number of the preferred configuration file format.
pub const FORMAT_VERSION_MAJOR: u32 = 0;

/// The minor version number of the preferred configuration file format.
pub const FORMAT_VERSION_MINOR: u32 = 1;

/// The tool section of a pyproject file.
#[derive(Debug, Deserialize)]
struct PyProjTool {
    /// The configuration we are looking for.
    publync: TomlValue,
}

/// The top-level structure of a pyproject file.
#[derive(Debug, Deserialize)]
struct PyProjTop {
    /// The tool section.
    tool: PyProjTool,
}

/// A major/minor version tuple.
#[derive(Debug, Deserialize, Serialize)]
pub struct FormatVersion {
    /// The major version number.
    major: u32,

    /// The minor version number.
    minor: u32,
}

impl FormatVersion {
    /// The major version number.
    #[inline]
    #[must_use]
    pub const fn major(&self) -> u32 {
        self.major
    }

    /// The minor version number.
    #[inline]
    #[must_use]
    pub const fn minor(&self) -> u32 {
        self.minor
    }
}

/// The configuration file metadata, e.g. the format version.
#[derive(Debug, Deserialize, Serialize)]
pub struct FormatMetadata {
    /// The format version.
    version: FormatVersion,
}

impl FormatMetadata {
    /// The format version.
    #[inline]
    #[must_use]
    pub const fn version(&self) -> &FormatVersion {
        &self.version
    }
}

/// Help deserialize the format version only.
#[derive(Debug, Deserialize)]
struct FormatOnlyTop {
    /// The format metadata, e.g. the version.
    format: FormatMetadata,
}

/// Parse the format.version.major/minor tuple.
///
/// # Errors
///
/// [`Error::FileParseNoFormatVersion`] on a missing or invalid format.version section.
fn extract_format_version(path: &Path, value: &TomlValue) -> Result<FormatVersion, Error> {
    // Let's hope we won't be cloning a whole lot of weird stuff here.
    match FormatOnlyTop::deserialize((*value).clone()) {
        Ok(top) => Ok(top.format.version),
        Err(_) => Err(Error::FileParseNoFormatVersion(format!(
            "{path}",
            path = path.display()
        ))),
    }
}

/// Override definitions for the project configuration.
#[derive(Debug, Default, Deserialize)]
pub struct Overrides {
    /// The build tool overrides.
    build: Option<BuildToolOverrides>,

    /// The sync tool overrides.
    sync: Option<SyncToolOverrides>,
}

impl Overrides {
    /// Parse the contents of the overrides configuration file.
    ///
    /// # Errors
    ///
    /// [`Error::FileParseToml`] if the text cannot be parsed as valid TOML.
    /// [`Error::FileParsePublync`] if the data cannot be deserialized as override definitions.
    #[inline]
    pub fn parse_config(path: &Path, contents: &str) -> Result<Self, Error> {
        let path_str = || format!("{path}", path = path.display());
        debug!(
            "Read {count} bytes from {path}",
            count = contents.len(),
            path = path.display()
        );
        let raw = toml::from_str::<TomlValue>(contents)
            .context("Could not parse the file contents as valid TOML")
            .map_err(|err| Error::FileParseToml(path_str(), err))?;
        let fmt_ver = extract_format_version(path, &raw)?;
        if fmt_ver.major > FORMAT_VERSION_MAJOR {
            return Err(Error::FileParsePublync(
                path_str(),
                anyhow!(
                    "Unsupported major format version {major}",
                    major = fmt_ver.major
                ),
            ));
        }
        Self::deserialize(raw).map_err(|err| {
            Error::FileParsePublync(
                path_str(),
                AnyError::new(err).context("Could not deserialize the publync configuration"),
            )
        })
    }

    /// Find a suitable configuration file, parse it.
    ///
    /// # Errors
    ///
    /// [`Error::NoConfig`] if no configuration was found in any of the expected files.
    /// [`Error::FileRead`] if a configuration file could not be read.
    ///
    /// Propagate errors from [`Overrides::parse_config`].
    #[inline]
    pub fn from_config(cfg: &Config) -> Result<Self, Error> {
        let ovpath = cfg.config_home().join("publync/overrides.d/overrides.toml");
        match fs::read_to_string(&ovpath) {
            Ok(contents) => Self::parse_config(&ovpath, &contents),
            Err(err) if err.kind() == ErrorKind::NotFound => Ok(Self::default()),
            Err(err) => Err(Error::FileRead(
                format!("{path}", path = ovpath.display()),
                err,
            )),
        }
    }
}

/// The project-specific configuration.
#[derive(Debug, Deserialize, Serialize)]
pub struct ProjectConfig {
    /// The format metadata, e.g. the version.
    format: FormatMetadata,

    /// The program to use for building the documentation.
    build: BuildTool,

    /// The program to use for syncing the documentation.
    sync: SyncTool,
}

impl ProjectConfig {
    /// The program to use for building the documentation.
    #[inline]
    #[must_use]
    pub const fn build(&self) -> &BuildTool {
        &self.build
    }

    /// The program to use for syncing the documentation.
    #[inline]
    #[must_use]
    pub const fn sync(&self) -> &SyncTool {
        &self.sync
    }

    /// Apply overrides to the configuration.
    #[inline]
    #[must_use]
    pub fn with_overrides(self, overrides: Overrides) -> Self {
        Self {
            format: self.format,
            build: if let Some(ovr) = overrides.build {
                self.build.with_overrides(ovr)
            } else {
                self.build
            },
            sync: if let Some(ovr) = overrides.sync {
                self.sync.with_overrides(ovr)
            } else {
                self.sync
            },
        }
    }

    /// Parse the TOML contents of the overrides configuration file.
    ///
    /// # Errors
    ///
    /// [`Error::FileParseToml`] if the text cannot be parsed as valid TOML.
    /// [`Error::FileParsePublync`] if the data cannot be deserialized as override definitions.
    #[inline]
    fn parse_toml_config(path: &Path, raw: toml::Value) -> Result<Self, Error> {
        let path_str = || format!("{path}", path = path.display());
        let fmt_ver = extract_format_version(path, &raw)?;
        if fmt_ver.major > 0 {
            return Err(Error::FileParsePublync(
                path_str(),
                anyhow!(
                    "Unsupported major format version {major}",
                    major = fmt_ver.major
                ),
            ));
        }
        Self::deserialize(raw)
            .map(|res| Self {
                format: FormatMetadata {
                    version: FormatVersion {
                        major: FORMAT_VERSION_MAJOR,
                        minor: FORMAT_VERSION_MINOR,
                    },
                },
                ..res
            })
            .map_err(|err| {
                Error::FileParsePublync(
                    path_str(),
                    AnyError::new(err).context("Could not deserialize the publync configuration"),
                )
            })
    }

    /// Parse the contents of a configuration file.
    ///
    /// # Errors
    ///
    /// [`Error::FileParseToml`] if the file contents was invalid TOML.
    /// [`Error::FileParsePublync`] if the file contents could not be deserialized.
    #[inline]
    pub fn parse_config(path: &Path, contents: &str) -> Result<Self, Error> {
        let path_str = || format!("{path}", path = path.display());
        debug!(
            "Read {count} bytes from {path}",
            count = contents.len(),
            path = path.display()
        );
        Self::parse_toml_config(
            path,
            toml::from_str::<TomlValue>(contents)
                .context("Could not parse the file contents as valid TOML")
                .map_err(|err| Error::FileParseToml(path_str(), err))?,
        )
    }

    /// Find a suitable configuration file, parse it.
    ///
    /// # Errors
    ///
    /// [`Error::NoConfig`] if no configuration was found in any of the expected files.
    /// [`Error::FileRead`] if a configuration file could not be read.
    ///
    /// Propagate errors from [`ProjectConfig::parse_config`].
    #[allow(clippy::missing_inline_in_public_items)]
    pub fn from_config_without_overrides(cfg: &Config) -> Result<Self, Error> {
        debug!(
            "Looking for publync configuration files in {path}",
            path = cfg.project_dir_str()
        );

        // Look for `publync.toml` first...
        {
            let lync_path = cfg.project_dir().join("publync.toml");
            match fs::read_to_string(&lync_path) {
                Ok(contents) => return Self::parse_config(&lync_path, &contents),
                Err(err) if err.kind() == ErrorKind::NotFound => (),
                Err(err) => {
                    return Err(Error::FileRead(
                        format!("{path}", path = lync_path.display()),
                        err,
                    ))
                }
            }
        }

        // ...and then `pyproject.toml` containing a [tool.publync] section.
        {
            let pyproj_path = cfg.project_dir().join("pyproject.toml");
            match fs::read_to_string(&pyproj_path) {
                Ok(contents) => {
                    debug!(
                        "Read {count} bytes from {path}",
                        count = contents.len(),
                        path = pyproj_path.display()
                    );
                    let raw = toml::from_str::<TomlValue>(&contents)
                        .context("Could not parse the file contents as valid TOML")
                        .map_err(|err| {
                            Error::FileParseToml(
                                format!("{path}", path = pyproj_path.display()),
                                err,
                            )
                        })?;
                    if let Ok(pyproj) = PyProjTop::deserialize(raw) {
                        return Self::parse_toml_config(&pyproj_path, pyproj.tool.publync);
                    }
                }
                Err(err) if err.kind() == ErrorKind::NotFound => (),
                Err(err) => {
                    return Err(Error::FileRead(
                        format!("{path}", path = pyproj_path.display()),
                        err,
                    ))
                }
            }
        }

        Err(Error::NoConfig(cfg.project_dir_str()))
    }

    /// Find a suitable configuration file, parse it.
    ///
    /// # Errors
    ///
    /// [`Error::NoConfig`] if no configuration was found in any of the expected files.
    /// [`Error::FileRead`] if a configuration file could not be read.
    ///
    /// Propagate errors from [`ProjectConfig::from_config_without_overrides`] and
    /// [`Overrides::from_config`].
    #[allow(clippy::missing_inline_in_public_items)]
    pub fn from_config(cfg: &Config) -> Result<Self, Error> {
        let proj_cfg = Self::from_config_without_overrides(cfg)?;
        Ok(proj_cfg.with_overrides(Overrides::from_config(cfg)?))
    }
}
