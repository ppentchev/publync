//! The project-specific configuration for the `publync` sync phase.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use serde_derive::{Deserialize, Serialize};

/// Override definitions for the rsync tool.
#[derive(Debug, Default, Deserialize)]
pub struct SyncRsyncOverrides {
    /// The remote username and host to sync to.
    remote: Option<String>,
}

/// Override definitions for the sync tool to be used.
#[derive(Debug, Default, Deserialize)]
pub struct SyncToolOverrides {
    /// The rsync tool.
    rsync: Option<SyncRsyncOverrides>,
}

/// The configuration for the rsync tool.
#[derive(Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct SyncRsyncConfig {
    /// The remote username and host to sync to.
    remote: String,
}

impl SyncRsyncConfig {
    /// The remote username and host to sync to.
    #[inline]
    #[must_use]
    pub fn remote(&self) -> &str {
        &self.remote
    }

    /// Apply overrides to the configuration.
    #[inline]
    #[must_use]
    pub fn with_overrides(self, overrides: SyncRsyncOverrides) -> Self {
        Self {
            remote: overrides.remote.unwrap_or(self.remote),
        }
    }
}

/// The program to use for syncing the built documentation.
#[derive(Debug, PartialEq, Eq, Deserialize, Serialize)]
#[non_exhaustive]
pub enum SyncTool {
    /// The rsync tool.
    #[serde(rename = "rsync")]
    Rsync(SyncRsyncConfig),
}

impl SyncTool {
    /// Apply overrides to the configuration.
    #[inline]
    #[must_use]
    pub fn with_overrides(self, overrides: SyncToolOverrides) -> Self {
        match self {
            Self::Rsync(cfg) => match overrides.rsync {
                Some(rsync) => Self::Rsync(cfg.with_overrides(rsync)),
                None => Self::Rsync(cfg),
            },
        }
    }
}
