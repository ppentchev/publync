//! The project-specific configuration for the `publync` build phase.

/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

use serde_derive::{Deserialize, Serialize};

/// The default Tox environment to invoke.
pub const DEFAULT_TOX_ENV: &str = "docs";

/// The default relative path to the generated documentation.
pub const DEFAULT_TOX_OUTPUT_PATH: &str = "site/docs";

/// The default Tox program to invoke.
pub const DEFAULT_TOX_PROG: &str = "tox";

/// Override definitions for the Tox runner.
#[derive(Debug, Default, Deserialize)]
pub struct BuildToxOverrides {
    /// The Tox environment to use.
    environment: Option<String>,

    /// The relative path to the generated documentation.
    #[serde(rename = "output-path")]
    output_path: Option<String>,

    /// The Tox program to use.
    program: Option<String>,
}

/// Override definitions for the build tool to be used.
#[derive(Debug, Default, Deserialize)]
pub struct BuildToolOverrides {
    /// The Python Tox test runner.
    tox: Option<BuildToxOverrides>,
}

/// Provide a default value for the Tox config environment.
///
/// Or at least, one that [`serde`] can understand.
fn default_tox_config_environment() -> String {
    DEFAULT_TOX_ENV.to_owned()
}

/// Provide a default value for the Tox config output path.
///
/// Or at least, one that [`serde`] can understand.
fn default_tox_config_output_path() -> String {
    DEFAULT_TOX_OUTPUT_PATH.to_owned()
}

/// Provide a default value for the Tox config program.
///
/// Or at least, one that [`serde`] can understand.
fn default_tox_config_program() -> String {
    DEFAULT_TOX_PROG.to_owned()
}

/// The configuration for the Python Tox test runner.
#[derive(Debug, PartialEq, Eq, Deserialize, Serialize)]
pub struct BuildToxConfig {
    /// The name of the Tox environment to invoke.
    #[serde(default = "default_tox_config_environment")]
    environment: String,

    /// The relative path to the generated documentation.
    #[serde(rename = "output-path", default = "default_tox_config_output_path")]
    output_path: String,

    /// The name of the Tox program to invoke.
    #[serde(default = "default_tox_config_program")]
    program: String,
}

impl Default for BuildToxConfig {
    #[inline]
    fn default() -> Self {
        Self {
            environment: default_tox_config_environment(),
            output_path: default_tox_config_output_path(),
            program: default_tox_config_program(),
        }
    }
}

impl BuildToxConfig {
    /// The name of the Tox environment to invoke.
    #[inline]
    #[must_use]
    pub fn environment(&self) -> &str {
        &self.environment
    }

    /// The relative path to the generated documentation.
    #[inline]
    #[must_use]
    pub fn output_path(&self) -> &str {
        &self.output_path
    }

    /// The name of the Tox program to invoke.
    #[inline]
    #[must_use]
    pub fn program(&self) -> &str {
        &self.program
    }

    /// Apply overrides to the configuration.
    #[inline]
    #[must_use]
    pub fn with_overrides(self, overrides: BuildToxOverrides) -> Self {
        Self {
            environment: overrides.environment.unwrap_or(self.environment),
            output_path: overrides.output_path.unwrap_or(self.output_path),
            program: overrides.program.unwrap_or(self.program),
        }
    }
}

/// The program to use for building the documentation.
#[derive(Debug, PartialEq, Eq, Deserialize, Serialize)]
#[non_exhaustive]
pub enum BuildTool {
    /// The Python Tox test runner.
    #[serde(rename = "tox")]
    Tox(BuildToxConfig),
}

impl BuildTool {
    /// Apply overrides to the configuration.
    #[inline]
    #[must_use]
    pub fn with_overrides(self, overrides: BuildToolOverrides) -> Self {
        match self {
            Self::Tox(cfg) => match overrides.tox {
                Some(tox) => Self::Tox(cfg.with_overrides(tox)),
                None => Self::Tox(cfg),
            },
        }
    }
}
